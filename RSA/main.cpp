//  main.cpp
//  RSA
//
//  Created by Sergiy on 06.06.17.

#include <iostream>
#include <math.h>
#include <string.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <cstdio>

using namespace std;


bool isPrime(long int prime);
long int getE( long int t );
long int getD( long int e, long int t );
long int encrypt( long int i, long int e, long int n );
long int decrypt(long int i, long int d, long int n );
long int gcd(long int a, long int b);

int main( )
{
    long int p, q, n, t, e, d;
    bool flag;
    double duration;
    string msg;
    
    long int encryptedText[1024];
    memset(encryptedText, 0, sizeof(encryptedText));
    
    long int decryptedText[1024];
    memset(decryptedText, 0, sizeof(decryptedText));
    
    // p и q
    
    do
    {
        cout << "Введите p :" << endl;
        cin >> p;
        
        flag = isPrime( p );

        if ( !flag )
        {
            cout << "\nВведенное число p не простое\n" << endl;
        }
    } while ( flag == false );

    do
    {
        cout << "Введите q :" << endl;
        cin >> q;
        flag = isPrime( q );

        if ( !flag )
        {
            cout << "\nВведеное число q не простое" << endl;
        }
    } while ( flag == false);
    
    //  n = p ⋅ q
    n = p * q;
    cout << "\nn = " << n << endl;
    
    // φ(n) = (p−1)⋅(q−1)
    t = ( p - 1 ) * ( q - 1 );
    cout << "φ(" << n << ") = " << t << endl;
    
    // e ( 1 < e < φ(n) ), НОД(φ(n),e) = 1
   
    e = getE( t );
    
    //  d, d ⋅ e ≡ 1 (mod φ(n))
    d = getD( e, t );
    

    cout << "\nПубиличный ключ (n = " << n << ", e = " << e << ")" << endl;

    cout << "Приватный ключ (n = " << n << ", d = " << d << ")" << endl;
    
    cout << "\nВведите текст для шифрования:" << endl;
    
    cin.ignore(1);
    
    getline( cin, msg );
    
    cout << "\nПолучено сообщение: " << msg << endl;
    
    // encryption
    
    clock_t start = clock();
    
    for (long int i = 0; i < msg.length(); i++)
    {
        encryptedText[i] = encrypt( msg[i], e, n);
    }
    
    
    cout << "\nЗашифрованное сообщение:" << endl;
    
    
    for ( long int i = 0; i < msg.length(); i++ )
    {
        cout << (char)encryptedText[i];
    }
    
    
    
    //decryption
    
    
    for (long int i = 0; i < msg.length(); i++)
    {
        decryptedText[i] = decrypt(encryptedText[i], d, n);
    }
    
    cout << "\n\nРасшифрованное сообщение:" << endl;
    
    for (long int i = 0; i < msg.length(); i++)
    {
        cout << (char)decryptedText[i];
    }
    
    duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
    
    cout<<"\n\nЗатраченное время:"<< duration <<'\n';
    
    cout << endl << endl;
    
    //system("PAUSE");
    
    return 0;
}

bool isPrime( long int prime)
{
    for ( long int i = 2; i <= (long int)sqrt(prime); i++)
    {
        if ( prime % i == 0 )
        {
            return false;
        }
    }
    
    return true;
}

long int gcd(long int a, long int b) {
    return b == 0 ? a : gcd(b, a % b);
}

long int getE( long int t )
{
    // e ( 1 < e < φ(n) ), НОД(φ(n),e) = 1
    
    long int e;
    
    for ( e = 2; e < t; e++ )
    {
        if (gcd( e, t ) == 1 )
        {
            return e;
        }
    }
    
    return -1;
}



long int getD( long int e, long int t)
{

    //    d * e ≡ 1 (mod φ(n))
    
    long int d;
    long int k = 1;
    
    while ( 1 )
    {
        k = k + t;
        
        if ( k % e == 0)
        {
            d = (k / e);
            return d;
        }
    }
    
}


long int encrypt( long int i, long int e, long int n )
{
    long int current, result;
    /*
     
     function modular_pow(base, index_n, modulus)
        c := 1
            for index_n_prime = 1 to index_n
                c := (c * base) mod modulus
     return c
     
     */
    current = i;
    result = 1;
    
    for ( long int j = 0; j < e; j++ )
    {
        result = result * current;
        result = result % n;
    }
    
    return result;
}





long int decrypt(long int i, long int d, long int n)
{
    long int current, result;
    
    current = i;
    result = 1;
    
    for ( long int j = 0; j < d; j++ )
    {
        result = result * current;
        result = result % n;
    }
    
    return result ;
}
